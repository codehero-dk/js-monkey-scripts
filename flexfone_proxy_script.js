// ==UserScript==
// @name         Codero Flexfone script
// @version      0.1
// @description  Convert tel: links to be handled with a Flexfone IP telephone
// @match        *
// @include      *
// ==/UserScript==

function findUpTag(el, attr) {
    while (el.parentNode) {
        el = el.parentNode;
        if (el[attr]) {
            return el;
        }
    }
    return null;
}

document.body.addEventListener( 'click', function (event) {
    var href = event.target.href;

    if (!href) {
        var closest = findUpTag(event.target, 'href');
        if (closest) {
            href = closest.href;
        }
    }
    if(href && href.match(/tel|callto/g)){
       event.preventDefault();
       call(href.replace("tel:","").replace("callto:","").replace("+45","").replace("+","00"));
    }
}, true );

function call(tel){
  if(GM_getValue("flexfonedevice") && GM_getValue("flexfoneusername") && GM_getValue("flexfonepassword")){
    var xmlhttp;

    GM_xmlhttpRequest({
      method: "GET",
      url:  "http://ameroservices.dk/flexfone_proxy/proxy.php?callee=" + encodeURIComponent(tel)  + "&device=" + encodeURIComponent(GM_getValue("flexfonedevice")) + "&username=" + encodeURIComponent(GM_getValue("flexfoneusername")) + "&password=" + encodeURIComponent(GM_getValue("flexfonepassword")),
      headers: {
        "User-Agent": "Mozilla/5.0",    // If not specified, navigator.userAgent will be used.
        "Accept": "text/xml"            // If not specified, browser defaults will be used.
      },
      onload: function(response) {
        GM_log([
          response.status,
          response.statusText,
          response.readyState,
          response.responseHeaders,
          response.responseText,
          response.finalUrl,
        ].join("\n"));
      }
    });
  }else{
     if(registerFlexfone()){
       call(tel);
     }
  }
}

function registerFlexfone(){
    var device = prompt("Please input flexfone device", "");
    if (device != null) {
        GM_setValue("flexfonedevice",device);
    }else{
        return false;
    }
    var username = prompt("Please input flexfone username", "");
    if (username != null) {
        GM_setValue("flexfoneusername",username);
    }else{
        return false;
    }
    var password = prompt("Please inoput flexfone password", "");
    if (password != null) {
        GM_setValue("flexfonepassword",password);
    }else{
        return false;
    }
    return true;
}
